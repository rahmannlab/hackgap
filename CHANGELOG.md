# Changelog

### 1.0.0

* Better support for compressed input files (gz (default), bzip2, xz)
* Faster k-mer counting with new consumer-producer model, so multiple input file readers can be used in parallel
* New faster hash table storage format (no more HDF5 or ZARR). Old k-mer counter hash tables are incompatible. Please count again
* Package is now built from metadata in `pyproject.toml`; the former `setup.py` has been removed
* Possibility to filter rare (gapped) k-mers before counting


### 0.11.0

* Initial public release at WABI 2022
